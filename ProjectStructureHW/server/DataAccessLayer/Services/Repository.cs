﻿using DataAccessLayer.Interfaces;
using DataAccessLayer.Models.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.Services
{
    public class Repository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly List<T> _collection;
        public Repository(List<T> collection)
        {
            _collection = collection;
        }
        public int Create(T entity)
        {
            //By convention collection is ordered by id
            var lastId = _collection.LastOrDefault()?.Id ?? 0;
            entity.Id = lastId + 1;

            _collection.Add(entity);
            return entity.Id;
        }

        public void Delete(int id)
        {
            var entity = _collection.SingleOrDefault(e => e.Id == id);
            _collection.Remove(entity);
        }

        public List<T> Get(Func<T, bool> filter = null)
        {
            return filter == null ? _collection : _collection.Where(filter).ToList();
        }

        public T Get(int id)
        {
            return _collection.SingleOrDefault(e => e.Id == id);
        }

        public void Update(T updatedEntity)
        {
            var entity = _collection.SingleOrDefault(e => e.Id == updatedEntity.Id);
            _collection.Remove(entity);
            _collection.Insert(updatedEntity.Id - 1, updatedEntity);
        }
    }
}
