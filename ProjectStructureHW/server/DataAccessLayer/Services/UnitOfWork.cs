﻿using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using DataAccessLayer.Models.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public UnitOfWork(DataContext context)
        {
            _context = context;
        }

        public IRepository<T> GetRepository<T>() where T : BaseEntity
        {
            return new Repository<T>(GetCollection<T>());
        }

        public bool SaveChanges()
        {
            //do some work
            return true;
        }

        public async Task<bool> SaveChangesAsync()
        {
            //do some asynchronous work
            return true;
        }

        private List<T> GetCollection<T>()
        {
            var type = typeof(T);

            if(type == typeof(Project))
            {
                return _context.Projects as List<T>;
            }

            if(type == typeof(Models.Task))
            {
                return _context.Tasks as List<T>;
            }

            if(type == typeof(Team))
            {
                return _context.Teams as List<T>;
            }

            if(type == typeof(User))
            {
                return _context.Users as List<T>;
            }

            throw new InvalidOperationException($"Collection of specified type {type} does not exist.");
        }
    }
}
