﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Models
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
