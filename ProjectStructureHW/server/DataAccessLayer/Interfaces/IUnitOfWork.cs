﻿using DataAccessLayer.Models.Abstractions;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T:BaseEntity;
        bool SaveChanges();
        Task<bool> SaveChangesAsync();
    }
}
