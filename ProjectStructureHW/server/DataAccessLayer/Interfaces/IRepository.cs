﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DataAccessLayer.Interfaces
{
    public interface IRepository<T> 
        where T:class
    {
        List<T> Get(Func<T,bool> filter = null);
        T Get(int id);
        int Create(T entity);
        void Update(T updatedEntity);
        void Delete(int id);
    }
}
