﻿using AutoMapper;
using Common.DTO;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Profiles
{
    public class MapperConfig:Profile
    {
        public MapperConfig()
        {
            CreateMap<ProjectDTO, Project>();
            CreateMap<Project, ProjectDTO>();

            CreateMap<Task, TaskDTO>();
            CreateMap<TaskDTO, Task>();

            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();

            CreateMap<UserDTO, User>();
            CreateMap<User,UserDTO>();
        }
    }
}
