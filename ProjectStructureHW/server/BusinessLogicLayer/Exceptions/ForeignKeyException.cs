﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Exceptions
{
    public class ForeignKeyException:Exception
    {
        public ForeignKeyException(Type sourceType, string propertyName, object propertyValue, Type targetType) : 
            base($"Property '{propertyName}'={propertyValue} of '{sourceType}' does not reference any instance of '{targetType}'") { }
        public ForeignKeyException(string message) : base(message) { }
    }
}
