﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using BusinessLogicLayer.Services.Internal;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Task> _repository;

        private readonly DeletionService _deletionService;
        private readonly ForeignKeyVerificationService _foreignKeyVerification;
        public TaskService(IUnitOfWork unitOfWork,
                           IMapper mapper) : base(mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<Task>();

            _deletionService = new DeletionService(_unitOfWork.GetRepository<Project>(),
                                                   _repository,
                                                   _unitOfWork.GetRepository<Team>(),
                                                   _unitOfWork.GetRepository<User>());

            _foreignKeyVerification = new ForeignKeyVerificationService(_unitOfWork.GetRepository<Project>(),
                                                                        _repository,
                                                                        _unitOfWork.GetRepository<Team>(),
                                                                        _unitOfWork.GetRepository<User>());
        }

        public int CreateTask(TaskDTO task)
        {
            if (task == null)
                throw new NullEntityException(typeof(TaskDTO));

            _foreignKeyVerification.CheckTaskForeignKeys(task);

            task.Id = _repository.Create(_mapper.Map<Task>(task));
            _unitOfWork.SaveChanges();

            return task.Id;
        }

        public void DeleteTask(int taskId)
        {
            if (GetTask(taskId) == null)
                throw new NotFoundException(typeof(Task), taskId);

            _deletionService.DeleteTaskRelatedData(taskId);
            _repository.Delete(taskId);

            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TaskDTO> GetAllTask()
        {
            return _repository.Get().Select(t => _mapper.Map<TaskDTO>(t));
        }

        public TaskDTO GetTask(int taskId)
        {
            Task entity = _repository.Get(taskId);

            if (entity == null)
                throw new NotFoundException(typeof(Task), taskId);

            var dto = _mapper.Map<TaskDTO>(entity);
            return dto;
        }

        public void UpdateTask(TaskDTO task)
        {
            if (task == null)
                throw new NullEntityException(typeof(TaskDTO));

            if (GetTask(task.Id) == null)
                throw new NotFoundException(typeof(Task), task.Id);

            var taskEntity = _mapper.Map<Task>(task);

            _repository.Update(taskEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
