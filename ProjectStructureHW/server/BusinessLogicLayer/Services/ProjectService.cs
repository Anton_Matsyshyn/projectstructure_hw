﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using BusinessLogicLayer.Services.Internal;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Project> _projectRepository;

        private readonly DeletionService _deletionService;
        private readonly ForeignKeyVerificationService _foreignKeyVerification;
        public ProjectService(IUnitOfWork unitOfWork,
                              IMapper mapper) : base(mapper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.GetRepository<Project>();

            _deletionService = new DeletionService(_projectRepository,
                                                   _unitOfWork.GetRepository<Task>(),
                                                   _unitOfWork.GetRepository<Team>(),
                                                   _unitOfWork.GetRepository<User>());

            _foreignKeyVerification = new ForeignKeyVerificationService(_projectRepository,
                                                                        _unitOfWork.GetRepository<Task>(),
                                                                        _unitOfWork.GetRepository<Team>(),
                                                                        _unitOfWork.GetRepository<User>());
        }
        public int CreateProject(ProjectDTO project)
        {
            if (project == null)
                throw new NullEntityException(typeof(ProjectDTO));

            _foreignKeyVerification.CheckProjectForeignKeys(project);

            project.Id = _projectRepository.Create(_mapper.Map<Project>(project));
            _unitOfWork.SaveChanges();

            return project.Id;
        }

        public void DeleteProject(int projectId)
        {
            if (GetProject(projectId) == null)
                throw new NotFoundException(typeof(Project), projectId);

            _deletionService.DeleteProjectRelatedData(projectId);
            _projectRepository.Delete(projectId);

            _unitOfWork.SaveChanges();
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _projectRepository.Get().Select(p => _mapper.Map<ProjectDTO>(p));
        }

        public ProjectDTO GetProject(int projectId)
        {
            Project entity = _projectRepository.Get(projectId);

            if (entity == null)
                throw new NotFoundException(typeof(Project), projectId);

            var dto = _mapper.Map<ProjectDTO>(entity);
            return dto;
        }

        public void UpdateProject(ProjectDTO project)
        {
            if (project == null)
                throw new NullEntityException(typeof(ProjectDTO));

            if (GetProject(project.Id) == null)
                throw new NotFoundException(typeof(Project), project.Id);

            var projectEntity = _mapper.Map<Project>(project);

            _projectRepository.Update(projectEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
