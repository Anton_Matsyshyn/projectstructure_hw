﻿using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services.Internal
{
    class DeletionService
    {
        private readonly IRepository<Project> _projectsRep;
        private readonly IRepository<Task> _taskRep;
        private readonly IRepository<Team> _teamsRep;
        private readonly IRepository<User> _usersRep;

        internal DeletionService(IRepository<Project> projectsRep,
                                 IRepository<Task> tasksRep,
                                 IRepository<Team> teamsRep,
                                 IRepository<User> usersRep)
        {
            _projectsRep = projectsRep;
            _taskRep = tasksRep;
            _teamsRep = teamsRep;
            _usersRep = usersRep;
        }

        internal void DeleteProjectRelatedData(int projectId)
        {
            var tasks = _taskRep.Get();

            if (tasks == null)
                return;

            for (int i = 0; i < tasks.Count; i++)
            {
                if (tasks[i].ProjectId == projectId)
                {
                    tasks.Remove(tasks[i]);
                    i--;
                }
            }
        }

        internal void DeleteTaskRelatedData(int taskId)
        {
            
        }

        internal void DeleteTeamRelatedData(int teamId)
        {
            var projects = _projectsRep.Get();
            var users = _usersRep.Get(u => u.TeamId == teamId);

            for(int i=0; i<projects.Count; i++)
            {
                if(projects[i].TeamId == teamId)
                {
                    DeleteProjectRelatedData(projects[i].Id);
                    projects.Remove(projects[i]);
                    i--;
                }
            }

            foreach(var user in users)
            {
                if(user.TeamId == teamId)
                {
                    user.TeamId = null;
                }
            }
        }

        internal void DeleteUserRelatedData(int userId)
        {
            var tasks = _taskRep.Get();
            var projects = _projectsRep.Get();

            for(int i=0; i<tasks.Count; i++)
            {
                if(tasks[i].PerformerId == userId)
                {
                    tasks.Remove(tasks[i]);
                    i--;
                }
            }

            for(int i=0; i<projects.Count; i++)
            {
                if(projects[i].AuthorId == userId)
                {
                    DeleteProjectRelatedData(projects[i].Id);
                    projects.Remove(projects[i]);
                    i--;
                }
            }
        }
    }
}
