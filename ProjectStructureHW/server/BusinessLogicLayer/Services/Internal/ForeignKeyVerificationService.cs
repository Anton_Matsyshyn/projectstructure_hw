﻿using BusinessLogicLayer.Exceptions;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Services.Internal
{
    class ForeignKeyVerificationService
    {
        private readonly IRepository<Project> _projectRep;
        private readonly IRepository<Task> _taskRep;
        private readonly IRepository<Team> _teamRep;
        private readonly IRepository<User> _userRep;

        internal ForeignKeyVerificationService(IRepository<Project> projectRep,
                                               IRepository<Task> taskRep,
                                               IRepository<Team> teamRep,
                                               IRepository<User> userRep)
        {
            _projectRep = projectRep;
            _taskRep = taskRep;
            _teamRep = teamRep;
            _userRep = userRep;
        }

        internal void CheckProjectForeignKeys(ProjectDTO project)
        {
            int teamId = project.TeamId;
            int userId = project.AuthorId;

            if (_teamRep.Get(t => t.Id == teamId).SingleOrDefault() == null)
                throw new ForeignKeyException(typeof(ProjectDTO), nameof(project.TeamId), teamId, typeof(Team));

            if (_userRep.Get(u => u.Id == userId).SingleOrDefault() == null)
                throw new ForeignKeyException(typeof(ProjectDTO), nameof(project.AuthorId), userId, typeof(User));
        }

        internal void CheckTaskForeignKeys(TaskDTO task)
        {
            int projectId = task.ProjectId;
            int userId = task.PerformerId;

            if (_projectRep.Get(p => p.Id == projectId).SingleOrDefault() == null)
                throw new ForeignKeyException(typeof(TaskDTO), nameof(task.ProjectId), projectId, typeof(Project));

            if (_userRep.Get(u => u.Id == userId).SingleOrDefault() == null)
                throw new ForeignKeyException(typeof(TaskDTO), nameof(task.PerformerId), userId, typeof(User));
        }

        internal void CheckTeamForeignKeys(TeamDTO team)
        {

        }

        internal void CheckUserForeignKeys(UserDTO user)
        {
            int? teamId = user.TeamId;

            if (teamId == null)
                return;

            if (_teamRep.Get(t => t.Id == teamId).SingleOrDefault() == null)
                throw new ForeignKeyException(typeof(UserDTO), nameof(user.TeamId), teamId, typeof(Team));
        }
    }
}
