﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using BusinessLogicLayer.Services.Internal;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<User> _repository;

        private readonly DeletionService _deletionService;
        private readonly ForeignKeyVerificationService _foreignKeyVerification;
        public UserService(IUnitOfWork unitOfWork,
                           IMapper mapper) : base(mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<User>();

            _deletionService = new DeletionService(_unitOfWork.GetRepository<Project>(),
                                                   _unitOfWork.GetRepository<Task>(),
                                                   _unitOfWork.GetRepository<Team>(),
                                                   _repository);

            _foreignKeyVerification = new ForeignKeyVerificationService(_unitOfWork.GetRepository<Project>(),
                                                                        _unitOfWork.GetRepository<Task>(),
                                                                        _unitOfWork.GetRepository<Team>(),
                                                                        _repository);
        }
        public int CreateUser(UserDTO user)
        {
            if (user == null)
                throw new NullEntityException(typeof(UserDTO));

            _foreignKeyVerification.CheckUserForeignKeys(user);

            user.Id = _repository.Create(_mapper.Map<User>(user));
            _unitOfWork.SaveChanges();

            return user.Id;
        }

        public void DeleteUser(int userId)
        {
            if (GetUser(userId) == null)
                throw new NotFoundException(typeof(User), userId);

            _deletionService.DeleteUserRelatedData(userId);
            _repository.Delete(userId);

            _unitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _repository.Get().Select(u => _mapper.Map<UserDTO>(u));
        }

        public UserDTO GetUser(int userId)
        {
            User entity = _repository.Get(userId);

            if (entity == null)
                throw new NotFoundException(typeof(User), userId);

            var dto = _mapper.Map<UserDTO>(entity);
            return dto;
        }

        public void UpdateUser(UserDTO user)
        {
            if (user == null)
                throw new NullEntityException(typeof(UserDTO));

            if (GetUser(user.Id) == null)
                throw new NotFoundException(typeof(User), user.Id);

            var userEntity = _mapper.Map<User>(user);

            _repository.Update(userEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
