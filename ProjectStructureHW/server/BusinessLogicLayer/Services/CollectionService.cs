﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace BusinessLogicLayer.Services
{
    public class CollectionService:BaseService, ICollectionService
    {
        private readonly IEnumerable<Project> _projects;
        private readonly IEnumerable<Task> _tasks;
        private readonly IEnumerable<Team> _teams;
        private readonly IEnumerable<User> _users;

        public CollectionService(IUnitOfWork unitOfWork,
                                 IMapper mapper) :base(mapper)
        {
            _projects = unitOfWork.GetRepository<Project>().Get();
            _tasks = unitOfWork.GetRepository<Task>().Get();
            _teams = unitOfWork.GetRepository<Team>().Get();
            _users = unitOfWork.GetRepository<User>().Get();
        }

        /// <summary>
        /// This is solution for 1st task from LINQ homework
        /// </summary>
        public Dictionary<int, int> GetProjectsIdAndTaskCount(int projectOwnerId)
        {
            if (_users.SingleOrDefault(u => u.Id == projectOwnerId) == null)
                throw new NotFoundException(typeof(User), projectOwnerId);

            var result = _projects.Where(p => p.AuthorId == projectOwnerId)
                                 .GroupJoin(_tasks,
                                            p => p.Id,
                                            t => t.ProjectId,
                                            (p, tList) => new
                                            {
                                                ProjectId = p.Id,
                                                TaskCount = tList.Count()
                                            })
                                 .ToDictionary(res => res.ProjectId, res => res.TaskCount);

            return result;
        }

        /// <summary>
        /// This is solution for 2nd task from LINQ homework
        /// </summary>
        public IEnumerable<TaskDTO> GetUsersTask(int userId)
        {
            if(_users.SingleOrDefault(u => u.Id == userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var result = _tasks.Where(t => t.PerformerId == userId &&
                                     t.Name.Count() < 45)
                               .Select(t => _mapper.Map<TaskDTO>(t));
            return result;
        }

        /// <summary>
        /// This is solution for 3d task from LINQ homework
        /// </summary>
        public IEnumerable<(int Id, string Name)> GetFinishedUserTasks(int userId)
        {
            if (_users.SingleOrDefault(u => u.Id == userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var filteredTasks = _tasks.Where(t => t.PerformerId == userId &&
                                            t.FinishedAt.Year == 2020 &&
                                            t.State == TaskState.Finished)
                                     .Select(t => (t.Id, t.Name));

            return filteredTasks;
        }

        /// <summary>
        /// This is solution for 4th task from LINQ homework
        /// </summary>
        public IEnumerable<TeamWithUsersDTO> GetTeamsWithUsers()
        {
            var result = _teams.GroupJoin(_users,
                                         t => t.Id,
                                         u => u.TeamId.GetValueOrDefault(),
                                         (t, uList) =>
                                         new TeamWithUsersDTO
                                         {
                                             TeamId = t.Id,
                                             TeamName = t.Name,
                                             Members = uList.Where(u => 2020 - u.Birthday.Year > 10)
                                                              .OrderByDescending(u => u.RegisteredAt)
                                                              .Select(u => _mapper.Map<UserDTO>(u))
                                         })
                                .Where(o => o.Members.Any(u => 2020 - u.Birthday.Year > 10));

            return result;
        }

        /// <summary>
        /// This is solution for 5th task from LINQ homework
        /// </summary>
        public IEnumerable<UserWithTasks> GetUsersWithTasks()
        {
            var result = _users.OrderBy(u => u.FirstName)
                               .GroupJoin(_tasks,
                                     u => u.Id,
                                     t => t.PerformerId,
                                     (u, tList) =>
                                     {
                                         u.Tasks = tList.OrderByDescending(t => t.Name.Length);
                                         return u;
                                     }
                                     )
                               .Select(u => new UserWithTasks 
                               { 
                                    User = _mapper.Map<UserDTO>(u),
                                    Tasks = u.Tasks.Select(t => _mapper.Map<TaskDTO>(t))
                               });

            return result;
        }

        /// <summary>
        /// This is solution for 6th task from LINQ homework
        /// </summary>
        public LastProjectAndTaskInfoDTO GetLastUserProject(int userId)
        {
            if (_users.SingleOrDefault(u => u.Id == userId) == null)
                throw new NotFoundException(typeof(User), userId);

            var result = _users.GroupJoin(_projects,
                                                    u => u.Id,
                                                    p => p.AuthorId,
                                                    (u, pList) =>
                                                    {
                                                        u.Projects = pList;
                                                        return u;
                                                    })
                                        .GroupJoin(_tasks,
                                                    u => u.Id,
                                                    t => t.PerformerId,
                                                    (u, tList) =>
                                                    {
                                                        u.Tasks = tList;
                                                        return u;
                                                    })
                                        .Where(u => u.Id == userId)
                                        .Select(u => GetProjectInfo(u))
                                        .FirstOrDefault();

            return result;
        }

        /// <summary>
        /// This is solution for 7th task from LINQ homework
        /// </summary>
        public IEnumerable<ProjectAndTeamInfoDTO> GetProjectWithTeam()
        {
            var result = _projects.GroupJoin(_tasks,
                                        p => p.Id,
                                        t => t.ProjectId,
                                        (p, tList) =>
                                        {
                                            p.Tasks = tList;
                                            return p;
                                        })
                                   .Join(_teams,
                                        p => p.TeamId,
                                        t => t.Id,
                                        (p, t) =>
                                        {
                                            p.Team = t;
                                            return p;
                                        })
                                   .GroupJoin(_users,
                                              p => p.TeamId,
                                              u => u.TeamId,
                                              (p, uList) =>
                                              {
                                                  p.Team.Users = uList;
                                                  return p;
                                              })
                                   .Select(p => new ProjectAndTeamInfoDTO
                                   {
                                       Project = _mapper.Map<ProjectDTO>(p),
                                       LongestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                                       ShortestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                                       TeammatesCount = p.Description.Length > 20 || p.Tasks.Count() < 3 ? p.Team.Users.Count() : default
                                   });

            return result;
        }

        private LastProjectAndTaskInfoDTO GetProjectInfo(User user)
        {
            return new LastProjectAndTaskInfoDTO
            {
                Author = _mapper.Map<UserDTO>(user),
                LastProject = _mapper.Map<ProjectDTO>(user.Projects?.GroupJoin(_tasks,
                                                                                p => p.Id,
                                                                                t => t.ProjectId,
                                                                                (p, tList) =>
                                                                                {
                                                                                    p.Tasks = tList;
                                                                                    return p;
                                                                                })
                                                                        .OrderBy(p => p.CreatedAt).LastOrDefault()),

                TaskCount = user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault()?.Tasks.Count() ?? 0,
                UnfinishedOrCanceledTaskCount = user.Tasks.Count(t => t.State != TaskState.Finished),
                LongestTask = _mapper.Map<TaskDTO>(user.Tasks?.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault())
            };
        }

    }
}
