﻿using AutoMapper;
using BusinessLogicLayer.Exceptions;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using BusinessLogicLayer.Services.Internal;
using Common.DTO;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Team> _repository;

        private readonly DeletionService _deletionService;
        private readonly ForeignKeyVerificationService _foreignKeyVerification;
        public TeamService(IUnitOfWork unitOfWork,
                           IMapper mapper) : base(mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<Team>();

            _deletionService = new DeletionService(_unitOfWork.GetRepository<Project>(),
                                                   _unitOfWork.GetRepository<Task>(),
                                                   _repository,
                                                   _unitOfWork.GetRepository<User>());

            _foreignKeyVerification = new ForeignKeyVerificationService(_unitOfWork.GetRepository<Project>(),
                                                                        _unitOfWork.GetRepository<Task>(),
                                                                        _repository,
                                                                        _unitOfWork.GetRepository<User>());
        }

        public int CreateTeam(TeamDTO team)
        {
            if (team == null)
                throw new NullEntityException(typeof(TeamDTO));

            _foreignKeyVerification.CheckTeamForeignKeys(team);

            team.Id = _repository.Create(_mapper.Map<Team>(team));
            _unitOfWork.SaveChanges();

            return team.Id;
        }

        public void DeleteTeam(int teamId)
        {
            if (GetTeam(teamId) == null)
                throw new NotFoundException(typeof(Team), teamId);

            _deletionService.DeleteTeamRelatedData(teamId);
            _repository.Delete(teamId);

            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {
            return _repository.Get().Select(t => _mapper.Map<TeamDTO>(t));
        }

        public TeamDTO GetTeam(int teamId)
        {
            Team entity = _repository.Get(teamId);

            if (entity == null)
                throw new NotFoundException(typeof(Team), teamId);

            var dto = _mapper.Map<TeamDTO>(entity);
            return dto;
        }

        public void UpdateTeam(TeamDTO team)
        {
            if (team == null)
                throw new NullEntityException(typeof(TeamDTO));

            if (GetTeam(team.Id) == null)
                throw new NotFoundException(typeof(Team), team.Id);

            var teamEntity = _mapper.Map<Team>(team);

            _repository.Update(teamEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
