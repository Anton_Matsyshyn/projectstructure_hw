﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstractions;
using Common.DTO;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TaskStateService : BaseService, ITaskStateService
    {
        public TaskStateService(IMapper mapper) : base(mapper) { }

        public IEnumerable<TaskStateDTO> GetTaskStates()
        {
            var result = new List<TaskStateDTO>();

            var enumValues = Enum.GetValues(typeof(TaskState));

            foreach(TaskState enumValue in enumValues)
            {
                result.Add(new TaskStateDTO { Id = (int)enumValue, Value = enumValue.ToString() });
            }

            return result;
        }
    }
}
