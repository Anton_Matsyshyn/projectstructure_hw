﻿using Common.DTO;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Interfaces
{
    public interface ICollectionService
    {
        Dictionary<int, int> GetProjectsIdAndTaskCount(int projectOwnerId);
        IEnumerable<TaskDTO> GetUsersTask(int userId);
        IEnumerable<(int Id, string Name)> GetFinishedUserTasks(int userId);
        IEnumerable<TeamWithUsersDTO> GetTeamsWithUsers();
        IEnumerable<UserWithTasks> GetUsersWithTasks();
        LastProjectAndTaskInfoDTO GetLastUserProject(int userId);
        IEnumerable<ProjectAndTeamInfoDTO> GetProjectWithTeam();
    }
}
