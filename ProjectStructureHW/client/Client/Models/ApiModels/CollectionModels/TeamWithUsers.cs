﻿using System.Collections.Generic;

namespace Client.Models.ApiModels.CollectionModels
{
    public class TeamWithUsers
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<User> Members { get; set; }
    }
}
