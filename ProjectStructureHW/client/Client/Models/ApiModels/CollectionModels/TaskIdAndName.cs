﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.ApiModels.CollectionModels
{
    public class TaskIdAndName
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
    }
}
