﻿
namespace Client.Models.ApiModels.CollectionModels
{
    public class ProjectAndTeamInfo
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int TeammatesCount { get; set; }
    }
}
