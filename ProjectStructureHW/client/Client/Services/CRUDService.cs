﻿using Client.Models.ApiModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using ApplicationTask = Client.Models.ApiModels.Task;
using ThreadTask = System.Threading.Tasks.Task;

namespace Client.Services
{
    public class CRUDService<T>
    {
        private readonly IConfigurationRoot _configuration;
        private readonly HttpClient _httpClient;
        public CRUDService(IConfigurationRoot configuration)
        {
            _configuration = configuration;

            _httpClient = new HttpClientFactory(configuration).GetClient();
            _httpClient.BaseAddress = new Uri(_httpClient.BaseAddress + GetEndpointFromType(typeof(T)) + "/");
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var response = await _httpClient.GetAsync("");
            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var entities = JsonConvert.DeserializeObject<IEnumerable<T>>(await content.ReadAsStringAsync());
                return entities;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }
        public async Task<T> GetEntityAsync(int id)
        {
            var response = await _httpClient.GetAsync(id.ToString());
            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var entity = JsonConvert.DeserializeObject<T>(await content.ReadAsStringAsync());
                return entity;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default;
        }
        public async ThreadTask CreateEntity(T entity)
        {
            var content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("", content);
            await CheckResponse(response);
        }
        public async ThreadTask UpdateEntity(T entity)
        {
            var content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync("", content);
            await CheckResponse(response);
        }
        public async ThreadTask DeleteEntity(int id)
        {
            var response = await _httpClient.DeleteAsync(id.ToString());
            await CheckResponse(response);
        }
        
        private async ThreadTask CheckResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return;

            var message = await response.Content.ReadAsStringAsync();

            throw new Exception(response.StatusCode + ":\t" + message);
        }
        private string GetEndpointFromType(Type type)
        {
            var endpoints = _configuration.GetSection("NetSettings:ApiEndpoints");

            if (type == typeof(Project))
            {
                return endpoints.GetSection("ProjectEndpoint").Value;
            }

            if (type == typeof(ApplicationTask))
            {
                return endpoints.GetSection("TaskEndpoint").Value;
            }

            if (type == typeof(Team))
            {
                return endpoints.GetSection("TeamEndpoint").Value;
            }

            if (type == typeof(User))
            {
                return endpoints.GetSection("UserEndpoint").Value;
            }

            return "";
        }
    }
}
